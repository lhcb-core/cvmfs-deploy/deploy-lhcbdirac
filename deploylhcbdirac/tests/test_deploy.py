import mock
import os
import shutil
import tempfile
import yaml

from depploylhcbdirac import deploy_lhcbdirac

from pytest import fixture

testDir = os.path.dirname(__file__)
configPath = os.path.join(testDir, 'config.yaml')


def mock_installVersion(basepath, version):
  """ Fake Install a new version
      basically just create a directory and a version link

      :param basepath: installation directory
      :param version: version to install
  """

  installDir = os.path.join(basepath, 'versions', '%s_randomStamp' % version)
  os.mkdir(installDir)
  os.symlink(installDir, os.path.join(basepath, version))


def test_confParse():
  """ Just tests the parsing of the config file """

  config = deploy_lhcbdirac.readConfig(configPath)

  assert os.path.abspath(config['basepath']) == os.path.abspath('/tmp')
  assert config['versions'] == set(['v1', 'v2', 'v3'])
  assert config['links'] == {'prod': 'v3', 'devel': 'v2'}


@fixture(scope="function")
def setupFakeInstall():
  """ Create a fake installation setup
      a directory, with a 'versions' subdirectory, and fake links and
      versions installed

      NOTE: a bit of randomness wouldn't harm
  """

  tmpDir = tempfile.mkdtemp()

  # Read the test config, change the basepath and write it to the temporary location
  with open(configPath, 'r') as configFile:
    config = yaml.safe_load(configFile)
    config['basepath'] = tmpDir
    with open(os.path.join(tmpDir, 'config.yaml'), 'w') as tmpConfigFile:
      yaml.dump(config, tmpConfigFile)

  versionsDir = os.path.join(tmpDir, 'versions')
  os.mkdir(versionsDir)

  # install 2 versions, one which we keep (v3), one that goes (v4)
  for ver in ('v3', 'v4'):
    realVerDir = os.path.join(versionsDir, '%s_randomStamp' % ver)
    os.mkdir(realVerDir)
    os.symlink(realVerDir, os.path.join(tmpDir, ver))

  # create two links, one to update, one to remove
  os.symlink(os.path.join(versionsDir, 'v4_randomStamp'), os.path.join(tmpDir, 'prod'))
  os.symlink(os.path.join(tmpDir, 'v3'), os.path.join(tmpDir, 'testing'))

  yield tmpDir

  shutil.rmtree(tmpDir)


@mock.patch('depploylhcbdirac.deploy_lhcbdirac.installVersion', side_effect=mock_installVersion)
def test_integration(mock_install, setupFakeInstall):
  """ Basically the best way to test that is to synchronize twice: the first time it should do something, not the second time"""
  tmpLocation = setupFakeInstall

  diff = deploy_lhcbdirac.synchronizeConfAndInstalled(os.path.join(tmpLocation, 'config.yaml'))

  # Make sure that things were done
  for todo in diff.itervalues():
    assert todo

  diff = deploy_lhcbdirac.synchronizeConfAndInstalled(os.path.join(tmpLocation, 'config.yaml'))

  # Make sure that nothing was done
  for todo in diff.itervalues():
    assert not todo
