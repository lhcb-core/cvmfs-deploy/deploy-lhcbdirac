import errno
import logging
import os
import shutil
import stat
import subprocess
import tempfile
import yaml
import sys

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def readConfig(configPath):
    """ Read the configuration file
        :param configPath: path to the yaml configuration file
        :returns: dict with ``basepath``, ``links`` and ``versions`` as keys
    """
    logger.info('ConfigPath: %s', configPath)
    with open(configPath, 'r') as configFile:
        config = yaml.safe_load(configFile)
        logger.debug("Configuration loaded %s", config)
        # convert versions to a set already
        config['versions'] = set(config['versions'])
        return config


def checkInstalledVersionsAndLinks(basepath):
    """ List the versions installed in the basepath and the links
     This method checks in fact was is installed in the 'versions' subdirectory
     because of the way LHCbDIRAC is now deployed on CVMFS (server install +
     manual symlink)

    :param basepath: basepath of the installation

    :returns: dict with keys:
               * versions: set of installed version
               * links: dict <link: version>

    """

    # In reality, that's where DIRAC will put the installation files
    versionDir = os.path.join(basepath, 'versions')
    if not os.path.exists(versionDir):
        return {'versions': set(), 'links': dict()}

    links = {}
    versions = set()

    # First check the installed versions
    for node in os.listdir(versionDir):

        nodePath = os.path.join(versionDir, node)

        # We are only interested in directory
        if not os.path.isdir(nodePath):
            logger.debug("Installed versions: skipping %s", nodePath)
            continue

        # The version has the form 'vxrypz-preu_<random>
        versionName = node.split('_')[0]

        logger.debug("Found version %s", versionName)

        versions.add(versionName)

    # Now check the links.
    # The annoying thing is that all the versions will appear as link
    # so we just have to ignore links that are just installed versions

    for node in os.listdir(basepath):

        nodePath = os.path.join(basepath, node)

        # We are only interested in link to directory
        if not (os.path.isdir(nodePath) and os.path.islink(nodePath)):
            logger.debug("Existing links: skipping %s", nodePath)
            continue

        # if the link is a link with the name of the version,
        # we skip it
        if node in versions:
            logger.debug("Existing links: skipping %s cause it is a version",
                         nodePath)
            continue

        # From now, we are sure that it is a link that we want

        # find the target
        targetPath = os.path.realpath(nodePath)
        targetVersion = os.path.basename(targetPath).split('_')[0]

        logger.debug("Found link %s pointing to %s", node, targetVersion)

        links[node] = targetVersion

    return {'versions': versions, 'links': links}


INSTALL_DIRAC_SCRIPT = """#!/bin/bash

set -e

cd %(basepath)s

curl -O -L https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/Core/scripts/dirac-install.py
chmod +x dirac-install.py
./dirac-install.py -v -r %(version)s -t server -l LHCb -e LHCb --createLink
# --scriptSymlink

source lhcbdirac %(version)s

pip install --trusted-host files.pythonhosted.org --trusted-host pypi.org --upgrade pip
pip install --trusted-host files.pythonhosted.org --trusted-host pypi.org ipython

# Download the tests and put them with the code
curl -L https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/-/archive/devel/LHCbDIRAC-devel.tar.gz | tar    --strip-components=1 -C %(version)s -xzf - LHCbDIRAC-devel/tests

"""

# owner can do everything
INSTALL_SCRIPT_PERMISSIONS = stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR


def installVersion(basepath, version):
    """ Install a new version
     We make the assumption that 'dirac-install.py' is available in the
     basepath

     :param basepath: installation directory
     :param version: version to install
    """

    # because dirac-install is shit, we need to change directory
    # so keep the current one so we can come back to it

    curDir = os.path.abspath(os.getcwd())

    try:
        installScriptFd, installScriptPath = tempfile.mkstemp()
        logger.debug("Creating install script in %s", installScriptPath)

        os.write(installScriptFd, (INSTALL_DIRAC_SCRIPT % {'basepath': basepath,
                                                          'version': version}).encode())
        os.close(installScriptFd)
        os.chmod(installScriptPath, INSTALL_SCRIPT_PERMISSIONS)
        logger.debug("Running %s", [installScriptPath])
        subprocess.check_call([installScriptPath])

    finally:
        # No matter what, go back to where we were
        os.chdir(curDir)
        try:
            os.unlink(installScriptPath)
        except BaseException:
            pass

SETUP_LHCBDIRAC_SCRIPT="""
#!/usr/bin/printf you must "source %s"\n

INSTALL_ROOT=%(install_root)s

# if the first command line option is a version use the version of
# LHCbDirac specified as argument
if [ $# -eq 1 ] ; then
  LHCBDIRAC_VERSION=$1
else
  LHCBDIRAC_VERSION=pro
fi

echo "Setting the environment for LHCbDIRAC ${LHCBDIRAC_VERSION}"


if [ -r ${INSTALL_ROOT}/${LHCBDIRAC_VERSION} ] ; then
   export DIRAC=${INSTALL_ROOT}/${LHCBDIRAC_VERSION}
else
   echo "${INSTALL_ROOT}/${LHCBDIRAC_VERSION} does not exist"
   exit 1
fi

source ${INSTALL_ROOT}/bashrc

"""
def generateSetup(basepath, filename="lhcbdirac"):
    """ Generate the script to setup the environment.
    Requires the top script to be created already.

     :param basepath: installation directory
    """

    if not os.path.exists(basepath):
        raise Exception("Install path %s does not exist" % basepath)

    with open(os.path.join(basepath, filename), "wt") as f:
         f.write(SETUP_LHCBDIRAC_SCRIPT % {'install_root': basepath})


def removeVersion(basepath, version):
    """ Remove a version
    In practice, it removes the symlinks and the folder it points to
    (normally in the 'versions' subdir)
    :param basepath: installation directory
    :param version: version to install
    """

    linkPath = os.path.join(basepath, version)

    versionPath = os.path.realpath(linkPath)

    # remove the link
    os.unlink(linkPath)

    # remove the actual version
    shutil.rmtree(versionPath)


def createLink(basepath, linkName, version):
    """Create a symlink pointing to the given version
    :param basepath: installation directory
    :param linkName: name of the link
    :param version: version to point to
    """

    # It is likely that the link already exists, so remove it first
    # and ignore error if it does not exist
    try:
        removeLink(basepath, linkName)
    except OSError as ex:
        if ex.errno == errno.ENOENT:
            pass

    linkPath = os.path.join(basepath, linkName)
    versionPath = os.path.join(basepath, version)
    os.symlink(versionPath, linkPath)


def removeLink(basepath, linkName):
    """
         Remove a symlink

         :param basepath: installation directory
         :param linkName: name of the link
    """

    linkPath = os.path.join(basepath, linkName)

    os.unlink(linkPath)


def compareConfAndInstalled(config, currentSetup):
    """ Compare what is installed with what the configuration file says
    :param config: content of the configuration file
    :param currentSetup: currently installed setup
    :returns: dictionnary with the following:
               * versionToInstall: sets of versions to install
               * versionToRemove: sets of versions to remove
               * linksToCreate: list of tuple (link name, target version)
               of symlink to create
               * linksToUpdate: list of tuple (link name, target version)
               of symlink to update
              * linksToRemove: set of symlink to remove
    """

    diff = {}

    versionsToInstall = config['versions'] - currentSetup['versions']
    diff['versionToInstall'] = versionsToInstall
    logger.info('Versions to install %s', versionsToInstall)

    versionsToRemove = currentSetup['versions'] - config['versions']
    diff['versionsToRemove'] = versionsToRemove
    logger.info('Versions to remove %s', versionsToRemove)

    existingLinks = set(currentSetup['links'])
    wantedLinks = set(config['links'])

    linksToCreate = wantedLinks - existingLinks
    diff['linksToCreate'] = [(link, config['links'][link])
                             for link in linksToCreate]
    logger.info('Links to create %s', linksToCreate)

    linksToRemove = existingLinks - wantedLinks
    diff['linksToRemove'] = linksToRemove
    logger.info('Links to remove %s', linksToRemove)

    linksToUpdate = [(link, config['links'][link])
                     for link in (wantedLinks & existingLinks)
                     if currentSetup['links'][link] != config['links'][link]]
    diff['linksToUpdate'] = linksToUpdate

    logger.info('Links to update %s', linksToUpdate)

    return diff


def synchronizeConfAndInstalled(configPath):
    """ Synchronizes what is installed with what the configuration file says

    :param configPath: path to the configuration file
    """

    config = readConfig(configPath)

    # Installation path
    basepath = config['basepath']

    # Checking whether the install scripts exists
    setupScriptName = "lhcbdirac"
    setupScriptPath = os.path.join(basepath, setupScriptName)
    if not os.path.exists(setupScriptPath):
        generateSetup(basepath, setupScriptName)

    currentSetup = checkInstalledVersionsAndLinks(basepath)

    diff = compareConfAndInstalled(config, currentSetup)

    for version in diff['versionToInstall']:
        installVersion(basepath, version)

    for version in diff['versionsToRemove']:
        removeVersion(basepath, version)

    for linkName, target in diff['linksToCreate']:
        createLink(basepath, linkName, target)

    for link in diff['linksToRemove']:
        removeLink(basepath, link)

    for linkName, target in diff['linksToUpdate']:
        createLink(basepath, linkName, target)

    return diff


def main():
    synchronizeConfAndInstalled(sys.argv[1])
