###############################################################################
# (c) Copyright 2019 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Module that manages the triggering of the LHCbDirac installation on CVMFSDev

@author: Ben Couturier & Stefan-Gabriel CHITIC
'''
import os
import logging
import yaml
from pykwalify.core import Core
from pykwalify.errors import SchemaError
import re
import sys
from lbmessaging.exchanges.CvmfsDevExchange import CvmfsDevExchange
from lbmessaging.exchanges.Common import get_connection


class Trigger:

    def __init__(self, args):
        self.log = logging.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.args = args

    def trigger(self):
        try:
            CvmfsDevExchange(get_connection(
                username=os.environ.get("RUSER", None),
                passwd=os.environ.get("RPASS", None)
            )).send_command(
                'deployLHCbDirac', self.args, priority=200)
        except Exception as e:
            self.log.error("Triggering: %s" % e)
            sys.exit(1)


class YamlManager:

    def __init__(self, configFile):
        self.log = logging.getLogger()
        self.log.setLevel(logging.DEBUG)
        self.configfile = configFile
        if not os.path.exists(self.configfile):
            raise Exception("Could not find config file %s" %
                            self.configfile)

    def loadConfig(self):
        ''' Just load the YAML config file '''
        with open(self.configfile) as f:
            config = yaml.load(f)
            return config

    def validateYAML(self):
        ''' Validates the yaml config file against the used schema '''
        script_dir = os.path.dirname(os.path.realpath(__file__))
        schema_file = os.path.join(script_dir, 'schema.yaml')
        self.log.warning("Using schema file %s" % schema_file)
        c = Core(source_file=self.configfile, schema_files=[schema_file])
        try:
            c.validate(raise_exception=True)
            return True
        except SchemaError as e:
            self.log.error("There was a problem validating with the file %s:"
                           "%s" % (self.configfile, str(e)))
            return False


def validate():
    if len(sys.argv) < 2:
        raise Exception("Please specify the configuration file!")
    configFile = sys.argv[1]
    validator = YamlManager(configFile)
    if validator.validateYAML():
        sys.exit(0)
    sys.exit(1)


def trigger():
    if len(sys.argv) < 2:
        raise Exception("No arguments passed!")
    Trigger(sys.argv[1:]).trigger()


if __name__ == '__main__':
    validate()
