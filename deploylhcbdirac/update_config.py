###############################################################################
# (c) Copyright 2019 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys
import yaml

def newProductionVersion(configPath, versionName):
    """
    Update the yaml configuration by adding a new version
    and pointing the prod link to it

    :param configPath: path to the yaml configuration file
    :param versionName: name of the version
    """

    with open(configPath, 'r') as configFile:
        config = yaml.safe_load(configFile)

    config['versions'].append(versionName)
    config['links']['prod'] = versionName

    with open(configPath, 'w') as configFile:
        yaml.dump(config, configFile)


def main():
    """ Just call newProductionVersion for the time being """
    newProductionVersion(*sys.argv[1:])


if __name__ == '__main__':
    sys.exit(main())
