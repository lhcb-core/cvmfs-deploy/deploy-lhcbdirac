# deploy-lhcbdirac

## Expected grammar

```
  basepath: /tmp/
  versions:
    - v1
    - v2
    - v3
  links:
    prod: v3
    devel: v2
```